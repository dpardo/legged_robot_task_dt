/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_contactGuardTerrain.hpp
 *
 *  Created on: April 24, 2016
 *      Author: depardo
 */

#ifndef _LRT_CONTACTGUARDTERRAIN_HPP_
#define _LRT_CONTACTGUARDTERRAIN_HPP_

#include <Eigen/Dense>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <optimization/constraints/GuardBase.hpp>
#include <legged_robot_task_dt/terrains/lrt_terrainMapBase.hpp>

namespace lrt{

template <class DIMENSIONS>
class LRTContactGuardTerrain: public  DirectTrajectoryOptimization::BaseClass::GuardBase<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::ContactConfiguration_t ContactConfiguration_t;
	typedef typename DIMENSIONS::EEPositionsDataMap_t EEPositionsDataMap_t;

  static const int dof_size = static_cast<int>(DIMENSIONS::DimensionsSize::DOF);

	LRTContactGuardTerrain(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lrd,
	                       const ContactConfiguration_t & fc, const double & tol, const double & ground_z):lr_dyn(lrd) {

	  double ground_min = ground_z-tol;
	  double ground_max = ground_z+tol;
	  contact_config = fc;
		feet_contact_id.clear();

		for(int i = 0 ; i < 4 ; i++) {
		    if(contact_config[i]) {
		        feet_contact_id.push_back(i);
		    }
		}
		num_feet_contact = feet_contact_id.size();

		this->SetSize(num_feet_contact);
		this->SetBounds(Eigen::VectorXd::Constant(num_feet_contact,ground_min),Eigen::VectorXd::Constant(num_feet_contact,ground_max));
		hyqTerrain_ = std::shared_ptr<LRTTerrainMapBase>(new LRTTerrainMapBase);
	}

	LeggedRobotDynamics<DIMENSIONS> lr_dyn;
	ContactConfiguration_t contact_config;
	std::vector<int> feet_contact_id;
	int num_feet_contact;

	GenearlizedCoordinates_t big_q;
	EEPositionsDataMap_t feetpose_inertia;
	EEPositionsDataMap_t feetpose_base;

	std::shared_ptr<LRTTerrainMapBase> lrt_Terrain_;

  void setHyQTerrainMap(std::shared_ptr<LRTTerrainMapBase> custom_terrain) {

    lrt_Terrain_.reset();
    lrt_Terrain_ = custom_terrain;
  }

	virtual ~LRTContactGuardTerrain(){};
	virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1) override;

};

template <class DIMENSIONS>
Eigen::VectorXd LRTContactGuardTerrain<DIMENSIONS>::evalConstraintsFct(const state_vector_t & x) {

  Eigen::VectorXd g(this->complete_vector_size);
	big_q = x.segment<dof_size>(0);
	lr_dyn.getEEPose(big_q,feetpose_inertia,feetpose_base);

	for(int i = 0 ; i < this->complete_vector_size ; i ++) {
	    double x_body = feetpose_inertia[feet_contact_id[i]](0);
			 g(i) = feetpose_inertia[feet_contact_id[i]](2) + lrt_Terrain_->elevationMap(x_body);//getTerrainHeight(x_body);
	}

	return g;
}

template <class DIMENSIONS>
const int LRTContactGuardTerrain<DIMENSIONS>::dof_size;

} // namespace lrt
#endif /* _LRT_CONTACTGUARDTERRAIN_HPP_ */
