/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * lrt_setOfConstraints.hpp
 *
 *  Created on: April 24, 2017
 *      Author: depardo
 */

#ifndef _LRT_SETOFCONSTRAINTS_HPP_
#define _LRT_SETOFCONSTRAINTS_HPP_

#include <optimization/constraints/ConstraintsBase.hpp>
#include <optimization/NumDiffDerivativesBase.hpp>
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <legged_robot_task_dt/constraints/lrt_singleConstraintBase.hpp>

namespace lrt {
 
template <class DIMENSIONS>
class LRTSetOfConstraints : public DirectTrajectoryOptimization::BaseClass::ConstraintsBase<DIMENSIONS> ,
                            public DirectTrajectoryOptimization::BaseClass::NumDiffDerivativesBase,
                            public std::enable_shared_from_this<LRTSetOfConstraints<DIMENSIONS>> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	static const int s_size = static_cast<int>(DIMENSIONS::DimensionsSize::STATE_SIZE);
	static const int c_size = static_cast<int>(DIMENSIONS::DimensionsSize::CONTROL_SIZE);
  static const int total_inputs_jacobian = static_cast<int>(DIMENSIONS::DimensionsSize::CONTROL_SIZE + DIMENSIONS::DimensionsSize::STATE_SIZE);

	LRTSetOfConstraints(std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > lr_dyn):
	  _lrt_Dynamics(lr_dyn) {
	}

	// To be called by the user before using it
	void initialize();
	virtual void initialize_num_diff();
	virtual void fx(const Eigen::VectorXd & in, Eigen::VectorXd & out);
	void addConstraint(std::shared_ptr<LRTSingleConstraintBase<DIMENSIONS> > _single_constraint);

	Eigen::VectorXd evalConstraintsFct(const state_vector_t& x,
	                                   const control_vector_t& u);
	Eigen::VectorXd evalLRTSetOfDerivatives(const state_vector_t& x,
	                                        const control_vector_t& u);
	Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t & x,
	                                              const control_vector_t & u);
	~LRTSetOfConstraints() {}

  std::shared_ptr<LRTSetOfConstraints<DIMENSIONS> > my_pointer;
	std::shared_ptr<LeggedRobotDynamics<DIMENSIONS> > _lrt_Dynamics;
	std::vector<std::shared_ptr<LRTSingleConstraintBase<DIMENSIONS> > > constraints_vector;

	std::vector<int> first_index_vector;
	std::vector<int> constraints_size;

	Eigen::VectorXd complete_vector_lb;
	Eigen::VectorXd complete_vector_ub;

  int complete_vector_size = 0;

	Eigen::VectorXd temp_jacobian_vector;
 	state_vector_t xd_local;

 	bool initialized_flag = false;
};

template <class DIMENSIONS>
void LRTSetOfConstraints<DIMENSIONS>::addConstraint(std::shared_ptr<LRTSingleConstraintBase<DIMENSIONS>> _single_constraint) {

  _single_constraint->UpdateDynamicPointer(_lrt_Dynamics);
	_single_constraint->InitializeParametersFromDynamics(_lrt_Dynamics);

	int ioc = complete_vector_size;
	int soc = 0;

	_single_constraint->GetSizeOfConstraint(soc);

	first_index_vector.push_back(ioc);
	constraints_size.push_back(soc);

	complete_vector_size += soc;

	Eigen::VectorXd temporal_lb , temporal_ub , c_lb, c_ub;

	temporal_lb = complete_vector_lb;
	temporal_ub = complete_vector_ub;

	_single_constraint->GetBounds(c_lb,c_ub);

	complete_vector_lb.resize(complete_vector_size);
	complete_vector_ub.resize(complete_vector_size);

	complete_vector_lb.segment(0,ioc) = temporal_lb;
	complete_vector_ub.segment(0,ioc) = temporal_ub;

	complete_vector_lb.segment(ioc,soc) = c_lb;
	complete_vector_ub.segment(ioc,soc) = c_ub;

	constraints_vector.push_back(_single_constraint);
}

template <class DIMENSIONS>
void LRTSetOfConstraints<DIMENSIONS>::initialize() {

  //this->g_size = complete_vector_size;
  this->setSize(complete_vector_size);

	//this->g_low = complete_vector_lb;
  this->setConstraintsLowerBound(complete_vector_lb);

  //this->g_up = complete_vector_ub;
  this->setConstraintsUpperBound(complete_vector_ub);

	// ToDo: Not using internal state sparsity
  // This initialization is exactly what DTO is expecting, dont use other sparsity setting!
    this->nnz_jac_ = total_inputs_jacobian * complete_vector_size;
    this->iRow_.resize(this->nnz_jac_);
    this->jCol_.resize(this->nnz_jac_);
    temp_jacobian_vector.resize(this->nnz_jac_);

    int k = 0;
    for(int col = 0 ; col < total_inputs_jacobian ; ++col) {
        for(int row = 0 ; row < complete_vector_size ; ++row) {
            this->iRow_(k) = row;
            this->jCol_(k) = col;
            k++;
        }
    }
		initialize_num_diff();
		initialized_flag = true;
}

template <class DIMENSIONS>
Eigen::VectorXd LRTSetOfConstraints<DIMENSIONS>::evalConstraintsFct(
		const state_vector_t& x, const control_vector_t& u) {

  if(!initialized_flag) {
      std::cout << "LRConstraint container not initialized!";
      std::exit(EXIT_FAILURE);

  }
	return(this->evalLRTSetOfDerivatives(x,u));

}

template <class DIMENSIONS>
Eigen::VectorXd LRTSetOfConstraints<DIMENSIONS>::evalLRTSetOfDerivatives(const state_vector_t& x, const control_vector_t& u) {

	Eigen::VectorXd outputs(complete_vector_size);

	_lrt_Dynamics->updateState(x);
	xd_local = _lrt_Dynamics->systemDynamics(x,u);

	for (size_t i = 0; i < constraints_vector.size() ; i++) {

		Eigen::VectorXd val;
		constraints_vector[i]->_x_dot = xd_local;
		constraints_vector[i]->evalLRTConstraint(x,u,val);
		outputs.segment(first_index_vector[i],constraints_size[i]) = val;
	}

	return(outputs);
}

template <class DIMENSIONS>
Eigen::VectorXd LRTSetOfConstraints<DIMENSIONS>::evalConstraintsFctDerivatives(
		const state_vector_t & x, const control_vector_t &u) {

	// ToDo :This should be provided by each constraint, here we are solving using NumDiff

	Eigen::VectorXd in_vector(total_inputs_jacobian);

	in_vector.template segment<s_size>(0) = x;
	in_vector.template segment<c_size>(s_size) = u;

	this->numDiff->df(in_vector,this->mJacobian);

	// column-major vector with the values of the Jacobian (as expected by dto)
	return (Eigen::Map<Eigen::VectorXd>(mJacobian.data(),this->nnz_jac_));
}

template<class DIMENSIONS>
void LRTSetOfConstraints<DIMENSIONS>::fx(const Eigen::VectorXd & inputs, Eigen::VectorXd & outputs) {

	state_vector_t   x = inputs.template segment<s_size>(0);
	control_vector_t u = inputs.template segment<c_size>(s_size);

	outputs = evalLRTSetOfDerivatives(x,u);
}

template<class DIMENSIONS>
void LRTSetOfConstraints<DIMENSIONS>::initialize_num_diff() {

	this->my_pointer = std::enable_shared_from_this<LRTSetOfConstraints<DIMENSIONS> >::shared_from_this();

	mJacobian.resize(complete_vector_size,total_inputs_jacobian);

	this->numdifoperator = std::shared_ptr<FunctionOperator>(new FunctionOperator(total_inputs_jacobian, complete_vector_size , this->my_pointer));

	this->numDiff = std::shared_ptr<Eigen::NumericalDiff<FunctionOperator> >( new Eigen::NumericalDiff<FunctionOperator>(*this->numdifoperator,std::numeric_limits<double>::epsilon()));

}

template<class DIMENSIONS>
const int LRTSetOfConstraints<DIMENSIONS>::c_size;
template<class DIMENSIONS>
const int LRTSetOfConstraints<DIMENSIONS>::s_size;
template<class DIMENSIONS>
const int LRTSetOfConstraints<DIMENSIONS>::total_inputs_jacobian;
} // namespace hyqdto
#endif /* LRT_HYQDTSETOFCONSTRAINTS_HPP_ */

