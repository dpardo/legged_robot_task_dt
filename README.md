# README #

This package provides base classes required to use DTO_PACKAGE together with a 
legged robot task.

Please check _This-repository_ with the complete set of packages and repositories
requiered for solving a DTO problem for the case of a legged robot using DTO_PACKAGE

You may find the complete documentation here:
